var AWS = require("aws-sdk");

const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  params: { Bucket: process.env.S3_BUCKET_NAME_IMAGES }
});

const uploadS3Image = async (stream, path) => {
  const awsResponse = await s3
    .upload({
      ACL: "public-read",
      Bucket: process.env.S3_BUCKET_NAME_IMAGES,
      Body: stream,
      Key: path
    })
    .promise();

    const imageName = path.split('/')[path.length - 1]

    return Object.assign({}, {name: imageName, source: awsResponse.Location})
}

module.exports = {
  uploadS3Image
}
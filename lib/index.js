/**
 * Created by Diego Velásquez on 12/06/2020.
 */

var slug = require('./slug');
var test = require('./test');
var aws = require('./aws');

module.exports = { slug, test, aws }
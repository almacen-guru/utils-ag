exports.getSlug = function (name) {
    var slug = (name).replace(/ - /g, " ");
    slug = (((((slug).toLowerCase()).trim()).replace(/ /g, "-")).replace(/[´'.()]/g, "")).replace(/ñ/g, "n");
    slug = slug.replace(/á/g, "a");
    slug = slug.replace(/é/g, "e");
    slug = slug.replace(/í/g, "i");
    slug = slug.replace(/ó/g, "o");
    slug = slug.replace(/ú/g, "u");
    return slug;
}

exports.testSlug = function () {
    console.log('dese slug.js')
}

exports.testSlug2 = function () {
    console.log('dese slug.jsasda')
}